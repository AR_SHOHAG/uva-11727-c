#include<stdio.h>

int main()
{
    int T, a, b, c, i, k;

    scanf("%d", &T);

    if(T<20){
        for(i=1; i<=T; ++i){
            scanf("%d%d%d", &a, &b, &c);
            k = find(a,b,c);
            printf("Case %d: %d\n", i, k);
        }
    }

    return 0;
}

int find(int A, int B, int C)
{
    if(A>B && A>C){
        if(B>C){
            return B;
        }
        else{
            return C;
        }
    }

    else if(B>A && B>C){
        if(A>C){
            return A;
        }
        else{
            return C;
        }
    }
    else if(C>A && C>B){
        if(A>B){
            return A;
        }
        else{
            return B;
        }
    }
}
